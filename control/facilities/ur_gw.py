# coding=utf-8
"""
Module for control communication functionality for the
"router"-interfaces to the Universal Robots controller.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import time
import traceback
import struct
import socket

import numpy as np

import pymoco.control

from .control_facility import ControlFacility

class URGwControlFacility(ControlFacility):
    """A controller facility class providing some simple common mechanism
    to receive LLC data from the LLC publisher and send joint
    updates to the LLC."""

    p_struct = struct.Struct('<BL6d')

    class Error(Exception):
        """Error class for thowing exceptions."""
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    def __init__(self, **kwargs):
        """Use the given urgw publisher for reading input and open a
        socket to write to the LLC on the given
        'gw_host' address and 'gw_in_port'. Broadcast if 'broadCast' is
        true."""
        #self._robUtils=robUtils
        ## Only broadcast if ordere to, and not local interface!
        ControlFacility.__init__(self, **kwargs)
        self._gw_socket = kwargs.get('gw_socket', None)
        if self._gw_socket is None:
            ## No externally handed socket
            self._log('No socket was given. Establishing a DGRAM socket.', 3)
            self._gw_in_port = kwargs.get('gw_in_port',
                                          pymoco.control.llc_in_port)
            self._gw_host = kwargs.get('gw_host', '127.0.0.1')
            self._gw_addr = (self._gw_host, self._gw_in_port)
            self._gw_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            # self._gw_socket.setsockopt(
            #     socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            self._gw_socket.connect(self._gw_addr)

    def _scale_dq_act(self, dqReqAct):
        """Scale an actuator kinematics joint increment in radians into
        allowed speed limits according to the interpolation period
        estimate from the LLC publisher."""
        dT = self._rob_fac.control_period
        dqMod = dqReqAct.copy()
        if self._log_level >= 5: logstr = 'Scaling flags: '
        if not self._rob_def.spd_lim_act is None:
            # Scale the step by a global factor so every component is
            # within limits.
            vreq = dqMod / dT
            vscaled = vreq / self._rob_def.spd_lim_act
            vfactor = np.max(np.abs(vscaled))
            if vfactor > 1.0:
                if self._log_level >=5:
                    logstr += 'v'
                dqMod /= vfactor
        if not self._rob_def.acc_lim_act is None:
            ## Clip the step, truncating it within bounds on acceleration.
            acc_lim_hi = dT * (
                self._v_tracked + self._rob_def.acc_lim_act * dT)
            acc_lim_low = dT * (
                self._v_tracked - self._rob_def.acc_lim_act * dT)
            dqModAcc = np.clip(dqMod, acc_lim_low, acc_lim_hi)
            if self._log_level >= 5 and (dqModAcc != dqMod).any():
                logstr += 'a'
            dqMod = dqModAcc
        if self._log_level >=5:
            if (dqMod == dqReqAct).all():
                logstr += '-'
            self._log(logstr, 5)
        return dqMod

    def _scale_dq_ser(self, dq_ser):
        """Scale a serial kinematics joint increment in radians into
        allowed speed limits according to the interpolation period
        estimate from the LLC publisher."""
        return self._rob_def.actuator2serial(
            self._scale_dq_act(
                self._rob_def.serial2actuator(dq_ser)))

    def _set_serial_config(self, q_ser, do_scale=True):
        """Set the joint configuration in serial kinematics and given
        in radians. No joint speed limit check is done."""
        if not self._initialized.is_set():
            self._initialized.wait()
        if q_ser is None:
            q_ser = self._q_tracked
        elif do_scale:
            dq_ser = q_ser - self._q_tracked
            dq_ser_scaled = self._scale_dq_ser(dq_ser)
            q_ser = self._q_tracked + dq_ser_scaled
        q_enc = self._rob_def.serial2encoder(q_ser)
        self._gw_socket.send(
            self.p_struct.pack(
                *([0x00, self._rob_fac.cycle_number] + q_enc.tolist())))
        # self._gw_socket.setsockopt(
        #     socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self._update_tracking(q_ser)
        if self._log_level >= 5:
            self._log('Sent joint position command packet of protocol "%d" '
                      + 'and for cycle number "%d"'
                      % (0, self._rob_fac.cycle_number), 5)

    def _set_serial_increment(self, dq_ser, do_scale=True):
        """Add the joint increment 'dq_ser' in radians in the serial
        kinematics to the most recent joint configuration. The
        increment is optionally scaled into allowed joint speed
        limits. Return value is the actually sent increment."""
        if not self._initialized.is_set():
            self._initialized.wait()
        dq_ser_scaled = dq_ser
        if do_scale:
            dq_ser_scaled = self._scale_dq_ser(dq_ser)
        self._set_serial_config(self._q_tracked + dq_ser_scaled,
                                do_scale=False)
        return dq_ser_scaled
