# coding=utf-8
"""
Module for setting up emulation of the Universal Robots UR-6-85-5-A
robot.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import struct
import socket
import time
import traceback

import site
import numpy as np
import math3d as m3d

import GameLogic

import pymoco
import pymoco.control
import pymoco.input
import pymoco.kinematics
import pymoco.robot

from pymoco.simulator.robot_emu import find_link_chain, obj_name

class URTCPServer(threading.Thread):
    """The TCP server which handles the aspects of the communication
    for the controller emulator."""

    def __init__(self, bind_host, gw_port, log_level=2):
        self._log_level = log_level
        self._bind_host = bind_host
        self._gw_port = gw_port
        threading.Thread.__init__(self)
        self.daemon = True
        self._server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server_socket.setsockopt(
            socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self._server_socket.setsockopt(
            socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        #self._server_socket.settimeout(0.25)
        try:
            self._server_socket.bind((self._bind_host, self._gw_port))
            self._server_socket.listen(1)
        except socket.error:
            self._log('Could not bind to socket!', 0)
            self._server_socket = None
        self._data_socket = None
        self._stop_flag = False
        self.start()
        
    def _log(self, msg, log_level = 1):
        """ Message for filtered logging."""
        if log_level <= self._log_level:
            print(str(log_level) + ' : ' + self.__class__.__name__
                  + ' ({}:{})::'.format(self._bind_host,self._gw_port)
                  + traceback.extract_stack(limit=2)[-2][2] + ' : ' + msg)

    def recv(self, n_bytes):
        """Try to receive 'n_bytes' from the socket, but close it upon
        exception."""
        if self._data_socket is None:
            time.sleep(0.1)
            return ''
        else:
            try:
                data = self._data_socket.recv(n_bytes)
            except socket.error as exc:
                self._log('Socket error "{}" on data socket. Closing.'
                          .format(str(exc)), 1)
                if not self._data_socket is None:
                    self._data_socket.shutdown(socket.SHUT_RDWR)
                    self._data_socket.close()
                    self._data_socket = None
                return ''
            else:
                self._log('Received data', 5)
                return data
        
    def send(self, data):
        """Send 'data' on the socket and handle exceptions."""
        if self._data_socket is None:
            return 0
        else:
            try:
                n_bytes = self._data_socket.send(data)
                self._data_socket.setsockopt(
                    socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
                self._log('Sent {} bytes on data socket.'.format(n_bytes), 5)
                return n_bytes
            except socket.error:
                self._log('Socket error on data socket. Closing.', 1)
                if not self._data_socket is None:
                    try:
                        self._data_socket.shutdown(socket.SHUT_RDWR)
                    except socket.error:
                        self._log('Socket error in shutdown in error handling'
                                  + ' of recv.',0)
                    self._data_socket.close()
                    self._data_socket = None
                return -1            

    def stop(self):
        """Stop the socket server, and close down gracefully."""
        self._log('Stopping')
        self._stop_flag = True
        #self.join()
        if not self._data_socket is None:
            self._data_socket.shutdown(socket.SHUT_RDWR)
            self._data_socket.close()
        if not self._server_socket is None:
            self._server_socket.shutdown(socket.SHUT_RDWR)
            self._server_socket.close()
        self._log('Stopped.')

    def run(self):
        """Worker method for the socket connection establishment and
        maintenance."""
        while not self._stop_flag:
            if self._server_socket is None:
                self._log('No server socket. Exitting!', 0)
                self.stop()
            elif self._data_socket is None:
                try:
                    self._log('Waiting for connection.', 4)
                    (self._data_socket, addr) = self._server_socket.accept()
                    self._data_socket.setsockopt(
                        socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
                    #self._data_socket.settimeout(0.005)
                except socket.timeout:
                    self._log('Timeout in data socket.', 5)
                except socket.error:
                    self._log('Possible shutdown!')
                else:
                    self._log('Got connection from "{}"'.format(str(addr)))
            else:
                time.sleep(0.5)

class URRouterTCPEmu(threading.Thread):
    """UR router emulator."""
    # // Structs for transmission of joint position data. The out struct  
    p_struct_out = struct.Struct('<BL6d6d')
    p_struct_in = struct.Struct('<BL6d')
  
    def __init__(self, rob_base_go, rob_def, bind_host='127.0.0.1',
                 gw_port=5002, log_level=2):
        threading.Thread.__init__(self)
        self.daemon = True
        self._stop_flag = False
        self._log_level = log_level
        # # Socket port for serving
        self._gw_port = gw_port
        self._bind_host = bind_host
        self._gw_addr = ((self._bind_host, self._gw_port))
        # // The game object for the robot base 
        self._base_go = rob_base_go
        self._rob_def = rob_def
        self._scene_to_meter_scale = self._base_go.get('scene_to_meter_scale', 1.0)
        self._meter_to_scene_scale = 1.0 / self._scene_to_meter_scale
        self._data_lock = threading.Lock()
        self._t_latest_emit = time.time()
        self._cycle_number = 0
        ## Take the q_home as encoder home
        self._q_arm = self._rob_def.encoder2serial(self._rob_def.q_home)
        self._last_q = self._q_arm.copy()
        # // Base transform and frame computer
        self._base_xform = m3d.Transform(
            m3d.Orientation(np.array(self._base_go.worldOrientation)),
            m3d.Vector(self._scene_to_meter_scale 
                       * np.array(self._base_go.worldPosition)))
        self._log('Found robot base at position {}'
                  .format(str(self._base_xform.pos)), 3)
        self._fc = pymoco.kinematics.FrameComputer(
            q=np.zeros(6), base_xform=self._base_xform,
            rob_def=self._rob_def, cache_in_frames=True, cache_out_frames=False)
        self._log('Kinematics report a tool position at {}'
                  .format(str(self._fc().pos)), 3)
        self._links = find_link_chain(self._base_go)
        # while link != None:
        #     self._links.append(link)
        #     link = findLink(link)
        self._log('Robot links: {}'.format(str(self._links)), 3)
        self._set_q(self._q_arm)
        self._gw_socket = URTCPServer(self._bind_host, 
                                      self._gw_port, 
                                      log_level=self._log_level)
        self._log('TCP Server listening on address "{}"'
                  .format(str(self._gw_addr)), 3)
        self._t_cur = time.time()
        self._t_last_log = self._t_cur
        self._period_est = 0.01
        self._t_old = self._t_cur - self._period_est
        
    def _log(self, msg, level = 1):
        """ Message for filtered logging."""
        if level <= self._log_level:
            print(str(level) + ' : ' + self.__class__.__name__
                  + ' ({})::'.format(str(self._gw_addr))
                  + traceback.extract_stack(limit=2)[-2][2] + ' : ' + msg)

    def _set_q(self, q_arm):
        """Set all robot elements according to the given
        arm-configuration in 'q_arm'."""
        self._fc.joint_angles_vec = q_arm
        frames = self._fc.get_in_frame(array=True)
        for link, frame in zip(self._links, frames):
            link.worldOrientation = frame[:3, :3].tolist()
            link.worldPosition = (self._meter_to_scene_scale * frame[:3, 3]).tolist()

    def emit_state(self):
        """Emit the current state of the robot, the arm-configuration,
        over the TCP socket."""
        if self._stop_flag:
            return
        # // Pack the stom package
        with self._data_lock:
            q_arm = self._q_arm.copy()
        vq_arm = (self._q_arm - self._last_q) / self._period_est
        # // Update cycle number
        self._cycle_number += 1
        # // Pack and send
        self._gw_socket.send(
            self.p_struct_out.pack(
                *([0x00, self._cycle_number] +
                  self._rob_def.serial2encoder(q_arm).tolist() +
                  vq_arm.tolist())))
        self._t_latest_emit = self._t_cur = time.time()
        self._period_est = (
            0.5 * self._period_est + 0.5 * (self._t_cur - self._t_old))
        if self._t_cur - self._t_last_log > 10:
            self._log(
                'Frame-rate est. : {:.2f}'.format(1.0 / self._period_est), 4)
            self._t_last_log = time.time()
        self._t_old = self._t_cur
        time.sleep(0.0025)

    def _test_recv_pos(self):
        """Test if a new arm-configuration can be received from a
        connected control client."""
        try:
            data = self._gw_socket.recv(1024)
            t_recv = time.time()
        except socket.error:
            pass
        else:
            if len(data) == self.p_struct_in.size:
                data_vec = self.p_struct_in.unpack(data)
                cycle_number_ack = data_vec[1]
                if cycle_number_ack != self._cycle_number:
                    self._log('Warning: Cycle number mis-match: {} != {}'
                              .format(cycle_number_ack, self._cycle_number), 3)
                q_arm = self._rob_def.encoder2serial(np.array(data_vec[2:]))
                self._set_q(q_arm)
                with self._data_lock:
                    self._last_q = self._q_arm
                    self._q_arm = q_arm
                    if self._log_level >= 5: 
                        self._log(
                            'Received position:' + str(self._q_arm), 5)
                resp_time = t_recv - self._t_latest_emit
                if  resp_time > 0.003:
                    self._log(
                        ('Warning: Deadline (3ms) exceeded. '
                        + 'Response time was {:.2f}ms')
                        .format(resp_time * 1000.0), 3)
            elif len(data) != 0:
                self._log(
                    ('Warning: Wrong packet size received. '
                     + 'Got {}, expected {} (or 0)')
                    .format(len(data), self.p_struct_in.size), 1)
                
    def run(self):
        """Worker method for cyclicly exchanging control commands with
        robot status."""
        self._stop_flag = False
        while not self._stop_flag:
            self._test_recv_pos()
        self._gw_socket.stop()
        self._log('Stopped.', 1)

    def stop(self):
        """Gracefully wait for the robot emulator to shut down."""
        self._log('Stopping')
        self._stop_flag = True
        self.join()

LOG_LEVEL = 2
__ur_emu__ = None
time_update = None

def init():
    """Main initialization of the emulated robot controller."""
    global __ur_emu__, time_update
    basename = obj_name('UR6855A.BasePlate')
    scene = GameLogic.getCurrentScene()
    ## Find base and set up emulator.
    base_go = None
    for obj in scene.objects:
        if obj.name.find(basename) >= 0:
            base_go = obj
            print('Found base object: "{}"'.format(base_go.name))
            break
    if not base_go is None:
        __ur_emu__ = URRouterTCPEmu(
            base_go,
            pymoco.robot.create_robot('UR5_ML_CR', cylinder_rolls=[0.0,0.0]),
            log_level=LOG_LEVEL)
        time_update = __ur_emu__.emit_state
        __ur_emu__.start()
    else:
        print('Error: No base object found, emulator not started!')

__shutdown_event__ = threading.Event()

def shutdown():
    """Main shutdown function for stopping the robot in the
    emulation."""
    if not __shutdown_event__.isSet():
        __shutdown_event__.set()
        print('Shutting down ur emu.')
        __ur_emu__.stop()
        ctrl = GameLogic.getCurrentController()
        exit_act = ctrl.actuators.get('exit', None)
        if not exit_act is None:
            ctrl.activate(exit_act)
