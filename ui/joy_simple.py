#!/usr/bin/env python3
# coding=utf-8
"""
Script for starting a simple joystick jogger application for control
of the UR robot over standard TCP port.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import atexit
import code
import argparse 

parser = argparse.ArgumentParser()
parser.add_argument('--rob_type', 
                    type=str, choices=['ur', 'sc15f', 'sh133'], default='ur')
parser.add_argument('--rob_sock_type', 
                    type=str, choices=['TCP','UDP'], default='TCP')
parser.add_argument('--rob_host', type=str, default='127.0.0.1')
parser.add_argument('--rob_port', type=int, default=5002)
parser.add_argument('--rob_in_port', type=int, default=20021)
parser.add_argument('--rob_out_port', type=int, default=20020)
parser.add_argument('--joy_path', type=str, default='/dev/input/js0')
args = parser.parse_args()


from pymoco.ui.joy_manager import JoyManager
from pymoco.controller_manager import ControllerManager

cm = ControllerManager(
    rob_type=args.rob_type, 
    rob_host=args.rob_host, 
    rob_port=args.rob_port, 
    rob_in_port=args.rob_in_port,
    rob_out_port=args.rob_out_port,
    rob_sock_type=args.rob_sock_type,
    log_level=2)

atexit.register(cm.stop)

jm = JoyManager([cm], start_index=0, js_dev_name=args.joy_path)

import __main__
if hasattr(__main__, '__file__'):
    import code
    code.interact(local=globals())

