# coding=utf-8
"""
Initialization for input package.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

# from llcpublisher import LLCPublisher
# from forcecontroller import ForceController

llc_out_port = 20020
